# Tinyfan

AVR Tiny based fan controller.

![](kicad/tinyfan/tinyfan_v2.jpg)

## Functionality

### Power

Board supports DC power from 5V to 50V. Internal buck connverter provides up to 500ma so if you use Fan from 5V (see jumper settings) use models accordingly. If you power Fan from DC you have to use Fan for the same voltage you provide to the board.

### Fans supported

Boards support 2/3 pin fans as well as PWM controlled 4 pin ones by configuration settings. See configuration. 5V Fan up to 500mA can be used with any voltage you supply to the board as it would be powered from board regulator, otherwise fan voltage has to match voltage supply. Board can control 2/3 pin fan RPMs through PWM control over mosfet if fan supports such mode.

### Temperature sensors supported

Currently board supported either 1 or 2 DS18B20 or similar digital sensors. Alternatively it can work with one analog NTC 100K or 10K sensor. See configuration section. When board is using 2 digital sensors then higher temrperature is used.

## configuration

### Hardware

Jumpers configuration:
- JP1 - solder this pin of you are powering FAN from DC power you provide to the board, in this case FAN voltage must be at least not lower than provided DC voltage. If JP1 is unsoldered FAN is powered by 5V board voltage and used fan current must not exceed 500mA.

### Software

Board supports UART configuration. FTDI usb adapter or arduino can be used to communicate to the board. Just connect Ground, Tx and Rx and Vcc pins on J3 connector on the same pins from adapter (Tx and Rx are marked as ones on adapter, not the board). Board is configured to communicate using 19200 bod 8N1 parameters. Commands:

- "t XXX" set trigger temperature level in Celcius (default 30)
- "T XXX" set maximum temprerature level in Celcius (default 50)
- "w XXX" set minimum RPM level from 0-255 (default 40)
- "W XXX" set maximum RPM level from 0-255 (default 255)
- "D X"   set sensors mode, 1 - digital, 0 - analog. (default 1)
- "1 x"   set 100K or 10K thermistor used for analog, 1 - 100K, 0 - 10K. (default 1)
- "f x"   set mode board controls fan, 1 - board uses PWM connection on 4 pin fans, 0 - board control power for 2/3 pin fans. (defualt 1)
- "b x"   set beta value for analog thermistor (default 3250 for HTC 100K)
- "p"     print current config in memory (note configuration will be lost in reboot until you isse save command)
- "P"     print current temeperature(s) detected by the board or error code
- "r"     reset config in EEPROM to default values (will only affect config after reboot)
- "s"     save current setting to EEPROM (you do not have to save after each change, one command saves all changes if any)

Boards sets RPM level to minimum RPM on trigger temperature reached, then RPM increased up to max setting when temprerature grows to max temperature proportional to the temperature. If more than one digital sensors detected then maximum temperature is used. Note if error condition is detected on sensors fan will be turned on on maximum settings. See board status.

To flash the board you need to use UPDI programmer and use P pin on J3 connector.

## Board status

Board status reported by status LED. If working condition is normal then LED lights permanently and short blinks show on how many sensors detected:

1. One blink shows 1 digital sensor is detected
2. Two blinks shows 2 digital sensors are detected
3. Three blinks shows analog thermistor is detected.

If problem is detected then LED turns off and flashed error code periodically:

1. No DS18B20 detected on the BUS
2. Too many DS18B20 devices (more than 2) connected to the BUS
3. Analog sensor configured, but analog connection is short (resistance is too low)
4. Analog sensor configured, but analog connection is disconnected (resistance is too high)
5. Unsupporeted OneWire device detected (only DS18S20 DS18B20 or DS1822 are supported)
6. Failure on bus communication (should not really happen, something really wrong)
7. CRC error on communication (same as before)

## Connection

Digital sensors can be connected into either J1 or J2 connectors (they are exactly the same, no difference which one is used first). P - provides power, S - signal, G - ground. If you use wired sensor red wire goes to P, yellow to S and black to G.

If you use analog thermistor make sure software configured accordingly. Thermistor can be connected btw S and G pins on either J1 or J2, polarity does not matter.
