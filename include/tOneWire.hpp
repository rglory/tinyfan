/*
 tOneWire.hpp - Tinyfan firmware for Attiny402/412.
 Copyright (c) 2021 Vyacheslav Kononenko.
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as 
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TONEWIRE_HPP
#define TONEWIRE_HPP

#include <Arduino.h>

class TOneWire {
public:
    static const int maxDeviceCount = 2;
    using Address = byte[8];

    TOneWire() : m_pin( NOT_A_PIN )
    {
    }
    
    TOneWire( uint8_t pin )
    {
        begin( pin );
    }

    void begin( uint8_t pin )
    {
        m_pin = pin;
        digitalWrite( m_pin, 0 );
        pinMode( m_pin, INPUT );
    }

    bool resetBus();
    bool readBit();
    byte read();

    void writeBit( bool b );
    void write( byte b );
    void write( const byte *b, uint16_t size );

    byte search();

    void beginConvert();
    void setResolution();
    byte getTemperature( uint8_t index, char &temp );

    uint8_t count() const { return m_deviceCount; }
    const byte *adress( uint8_t index ) const { return m_devices[index]; }

private:
    uint8_t m_pin;
    Address m_devices[maxDeviceCount];
    uint8_t m_deviceCount = 0;
};

#endif // TONEWIRE_HPP
