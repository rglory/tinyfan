/*
 tOneWire.cpp - Tinyfan firmware for Attiny402/412.
 Copyright (c) 2021 Vyacheslav Kononenko.
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as 
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <tOneWire.hpp>
#include <util/crc16.h>
#include <common.hpp>

void TOneWire::writeBit( bool one )
{
    noInterrupts();
    pinMode( m_pin, OUTPUT );
	delayMicroseconds( one ? 6 : 60 );
    pinMode( m_pin, INPUT );
	interrupts();
	delayMicroseconds( one ? 64 : 5 );
}

bool TOneWire::resetBus()
{
    int tries = 0;
    while( digitalRead( m_pin ) == 0 ) // wait for the bus to come to state 1
    {
        if( ++tries == 4 ) return false;
    	delayMicroseconds( 50 );
    }
    pinMode( m_pin, OUTPUT );
	delayMicroseconds( 480 );
    pinMode( m_pin, INPUT );
	delayMicroseconds( 70 );
	if( digitalRead( m_pin ) ) return false;
	delayMicroseconds( 410 );
    return true;
}

void TOneWire::beginConvert()
{
    resetBus();
    write( 0xCC ); // skip ROM
    write( 0x44 ); // start conversion
}

void TOneWire::setResolution()
{
    resetBus();
    write( 0xCC ); // skip ROM
    write( 0x4E ); // write scrathpad
    write( 0x7F ); // Th
    write( 0x80 ); // Tl
    write( 0x1F ); // 9 bit resolution
}


byte TOneWire::getTemperature( uint8_t index, char &temp )
{
    if( not resetBus() ) return errNoDeviceOnBus;
    write( 0x55 ); // match ROM
    write( m_devices[index], 8 );
    write( 0xBE ); // read scrathpad
    byte crc = 0;
    char ltemp = 0;
    for( int i = 0; i < 8; ++i ) {
        byte b = read();
        switch( i ) {
            case 0 : ltemp |= b >> 4; break;
            case 1 : ltemp |= b << 4; break;
        }
        crc = _crc_ibutton_update( crc, b );
    }
    if( crc != read() ) 
        return errBadCrc;
    temp = ltemp;
    return errNoError;
}


byte TOneWire::search()
{
    memset( m_devices, 0,  16 );
    uint8_t conflict = 0;
    for( m_deviceCount = 0; m_deviceCount < maxDeviceCount; ) {
        if( !resetBus() ) return errNoDeviceOnBus; // No devices on the bus
        write( 0xF0 );   // search ROM
        byte *addr = m_devices[m_deviceCount];
        byte crc = 0;
        byte v = 1;
        for( int i = 0; i < 64; ++i ) {
            bool bit = readBit();
            bool cbit = readBit();
            if( bit and cbit ) 
                return errCommunicationFault; // Weird error, should not happen, reset should detect no devices
            if( bit == cbit ) {
                if( ++conflict > 2 ) // second conflit, too many devices
                    return errTooManyDevices;
                bit = m_deviceCount; // conflict, using bit for device
            }
            writeBit( bit );
            if( bit ) *addr |= v;
            if( (v <<= 1) == 0 ) {
                v = 1;
                if( i == 7 ) {
                    switch( *addr ) {
                        case 0x10 : // DS18S20
                        case 0x28 : // DS18B20
                        case 0x22 : // DS1822
                            break;
                        default: return errUnsupportedDevice; // unsupported device family
                    }
                }
                if( i == 63 and crc != *addr ) return errBadCrc; // crc error on address
                crc = _crc_ibutton_update( crc, *addr++ );
            }
        }
        ++m_deviceCount;
        if( conflict == 0 ) break; // only one device
    }
    return 0;
}

bool TOneWire::readBit()
{
	noInterrupts();
    pinMode( m_pin, OUTPUT );
	delayMicroseconds( 6 );
    pinMode( m_pin, INPUT );
	delayMicroseconds( 9 );
	bool r = digitalRead( m_pin );
	interrupts();
	delayMicroseconds( 55 );
	return r;
}

byte TOneWire::read()
{
    byte r = 0;
    for( byte mask = 1; mask; mask <<= 1 )
        if( readBit() ) r |= mask;
    return r;
}

void TOneWire::write( byte b )
{
    for( byte mask = 1; mask; mask <<= 1 )
        writeBit( b & mask );
}

void TOneWire::write( const byte *b, uint16_t size )
{
    while( size-- ) write( *b++ );
}
