/*
 main.cpp - Tinyfan firmware for Attiny402/412.
 Copyright (c) 2021 Vyacheslav Kononenko.
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as 
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <Arduino.h>
#include <common.hpp>
#include <tOneWire.hpp>
#include <EEPROM.h>

#ifdef MILLIS_USE_TIMERA0
#error "This firmware takes over TCA0 - please use a different timer for millis"
#endif

#define TINYFAN_VERSION "2.1"

constexpr float analogResistor100 = 100.0f; // 100K resistor
constexpr float analogResistor10  = 10.0f;  // 10K resistor

const uint8_t pinOneWire = PIN_PA2; // pin used of 1Wire communication or analog read
const uint8_t pin4K_ENA  = PIN_PA1; // pin used of 1Wire communication or analog read
const uint8_t pin10K_ENA = PIN_PA3; // pin used of 1Wire communication or analog read
const uint8_t pinFAN     = PIN_PA4; // pin to control FAN power
const uint8_t pinPWM     = PIN_PA5; // pin to control FAN PWM
const uint8_t pinLED     = PIN_PB0; // pin to control status LED
const uint8_t pinLEDFAN  = PIN_PB1; // pin to control fan    LED

TOneWire wire;

struct Config {
    bool    useDS       = true;
    int8_t  triggerTemp =   30;
    int8_t  fullTemp    =   50;
    uint8_t minPWM      =   40;
    uint8_t maxPWM      =  255;
    uint16_t beta       = 3950;
    bool    use100K     = true;
    bool    usePWM      = true;
};

Config config;

byte errorCode = 0;
int8_t temperatures[2] = { -127, -127 };

void loadConfig()
{
    auto ptr = reinterpret_cast<uint8_t *>(&config);
    for( byte i = 0; i < sizeof(Config); ++i ) {
        auto v = EEPROM.read( i );
        if( i == 0 and v == 0xFF ) return; // EEPROM uninitialized, use default values
        ptr[i] = v;
    }
}

void saveConfig()
{
    auto ptr = reinterpret_cast<uint8_t *>(&config);
    for( byte i = 0; i < sizeof(Config); ++i )
        EEPROM.update( i, ptr[i] );
}

void controlLed();

byte readAnalogResistance( float *to ) // returns resistance in K
{
    auto v = analogRead( pinOneWire );
    if( v < 16 ) return errNTCshort; 
    if( v > 1016 ) return errNTCdisconnected;
    if( to ) {
        auto constexpr rpar = float( ( double( analogResistor100 ) * analogResistor10 ) / ( double( analogResistor100 ) + analogResistor10 ) );
        *to = ( config.use100K ? analogResistor100 : rpar ) / ( 1023.0f / v - 1 );
    }
    return 0;
}

void initTherms()
{
    if( config.useDS ) {
        errorCode = wire.search();
        if( errorCode == 0 ) {
            wire.setResolution();
        }   
    } else {
        errorCode = readAnalogResistance( nullptr );
    }
    controlLed();
}

void setup() 
{
    Serial.begin( 19200 );

    pinMode( pinLED, OUTPUT ); // LED status control
    digitalWrite( pinLED, 0 );
    pinMode( pinOneWire, INPUT );

    loadConfig();

    if( config.useDS ) {
        wire.begin( pinOneWire );
        pinMode( pin4K_ENA, OUTPUT ); 
        digitalWrite( pin4K_ENA, 1 );
    } else {
        if( not config.use100K ) {
            pinMode( pin10K_ENA, OUTPUT ); 
            digitalWrite( pin10K_ENA, 1 );
        }
    }

    pinMode( pinLEDFAN, OUTPUT ); // LED status control
    digitalWrite( pinLEDFAN, 0 );

    pinMode( pinFAN, OUTPUT );  // FAN enable
    digitalWrite( pinFAN, 0 );

    pinMode( pinPWM, OUTPUT );  // PWM control
    digitalWrite( pinPWM, 0 );

    if( config.usePWM )
        digitalWrite( pinFAN, 0 );
    else
        digitalWrite( pinPWM, 1 );


    initTherms();

    // configure TCA0 for 31KHz PWM
    TCA0.SPLIT.CTRLA = (config.usePWM ?  TCA_SPLIT_CLKSEL_DIV2_gc : TCA_SPLIT_CLKSEL_DIV64_gc) | (TCA_SINGLE_ENABLE_bm);

    // enable PIT interrupt to update status LED
    RTC.CLKSEL = RTC_CLKSEL_INT1K_gc;
    RTC.PITINTCTRL = 1;
    RTC.PITCTRLA   = RTC_PITEN_bm | RTC_PERIOD_CYC32_gc;

    Serial.println( "Tinyfan v" TINYFAN_VERSION " started" );
}

enum SerialErrorCode {
    serrSuccess,
    serrSyntax,
    serrUnknownCommand,
    serrBufferOverflow,
    serrMissingParameter
};

inline
void sendCode( SerialErrorCode code )
{
    if( code ) {
        Serial.print( 'E' );
        Serial.println( code );
    }
    else
        Serial.println( "OK" );
}

const int commandSize = 32;
char command[commandSize];
int pcommand = 0;

inline
void printConfig()
{
    Serial.println( "   verison:\t" TINYFAN_VERSION );
    Serial.print( "   use DS:\t" );
    Serial.println( config.useDS ? "true" : "false" );
    Serial.print( "   100K:\t\t" );
    Serial.println( config.use100K ? "true" : "false" );
    Serial.print( "   use PWM:\t" );
    Serial.println( config.usePWM ? "true" : "false" );
    Serial.print( "   th beta:\t" );
    Serial.println( config.beta );
    Serial.print( "   trigger Temp:\t" );
    Serial.println( config.triggerTemp );
    Serial.print( "   full Temp:\t" );
    Serial.println( config.fullTemp );
    Serial.print( "   min PWM:\t" );
    Serial.println( config.minPWM );
    Serial.print( "   max PWM:\t" );
    Serial.println( config.maxPWM );
}

inline
void printTemps()
{
    if( errorCode ) {
        Serial.print( "error: " );
        Serial.println( errorCode );
        return;
    }
    Serial.print( "  T0:\t" );
    Serial.println( temperatures[0] );
    if( config.useDS ) {
        Serial.print( "  T1:\t" );
        Serial.println( temperatures[1] );
    }
}

inline
void resetConfig()
{
    EEPROM.update( 0, 0xFF );
}

template<typename T>
SerialErrorCode setConfigValue( T &v, int par )
{
    if( par < 0 ) return serrMissingParameter;
    v = par;
    return serrSuccess;
}

void processCommand()
{
    static SerialErrorCode code = serrSuccess;
    while( Serial.available() ) {
        char c = Serial.read();
        if( c != '\n' and c != '\r' ) {
            if( pcommand == commandSize ) 
                code = serrBufferOverflow;
            else 
                command[pcommand++] = c;
            continue;
        }      
        if( pcommand == 0 ) continue;

        if( code == serrSuccess and pcommand > 1 and command[1] != ' ' ) 
            code = serrSyntax;

        if( code == serrSuccess ) {
            int p = -1;
            if( pcommand > 2 ) p = atoi( command + 2 );
            switch( command[0]) {
                case 't' : code = setConfigValue( config.triggerTemp, p ); break;
                case 'T' : code = setConfigValue( config.fullTemp, p );    break;
                case 'w' : code = setConfigValue( config.minPWM, p );      break;
                case 'W' : code = setConfigValue( config.maxPWM, p );      break;
                case 'D' : code = setConfigValue( config.useDS, p );       break;
                case '1' : code = setConfigValue( config.use100K, p );     break;
                case 'f' : code = setConfigValue( config.usePWM, p );      break;
                case 'b' : code = setConfigValue( config.beta, p );        break;
                case 'p' : printConfig(); break;
                case 'P' : printTemps();  break;
                case 'r' : resetConfig(); break;
                case 's' : saveConfig();  break;
                default : code = serrUnknownCommand;
            }
        }
        sendCode( code );
        code = serrSuccess;
        pcommand = 0;
    }
}

inline
void setFan( uint8_t val )
{
    analogWrite( pinLEDFAN, val );
    if( config.usePWM ) {
        analogWrite( pinPWM, val );
        digitalWrite( pinFAN, 0 );
    } else {
        analogWrite( pinFAN, 255 - val );
        digitalWrite( pinPWM, 1 );

    }
}

void controlFan()
{
    static char temps[10] = {};
    static byte tidx = 0;

    auto ltime = millis();
    static decltype(ltime) time = 0;

    if( ltime - time < 100 ) return;
    time = ltime;
    if( errorCode ) {
        setFan( config.maxPWM );
        return;
    }
    temps[tidx++] = max( temperatures[0], temperatures[1] );
    if( tidx >= sizeof( temps) ) tidx = 0;

    char temp = 0;
    for( auto t : temps ) 
        if( temp < t ) temp = t;

    if( temp >= config.fullTemp ) {
        setFan( config.maxPWM );
        return;
    }
    if( temp < config.triggerTemp ) {
        setFan( 0 );
        return; 
    }
    byte v = config.maxPWM - (unsigned long)(config.maxPWM - config.minPWM) * 
                             ( config.fullTemp - temp ) / ( config.fullTemp - config.triggerTemp );
    setFan( v );
}

byte ledToReport = 0;
bool ledPauseOn  = false;

ISR(RTC_PIT_vect)
{
    RTC.PITINTFLAGS = 1;
    static auto time = millis();
    if( ledToReport == 0 )
        return;
    auto ltime = millis();
    unsigned delta = ltime - time;
    static byte reported = 0;
    if( reported >= 2 * ledToReport ) {
        if( delta < 1200 ) return;
        reported = 0;
        time = ltime;
    }
    if( delta < 100 ) return;
    digitalWrite( pinLED, reported++ % 2 == ledPauseOn );
    time = ltime;            
}

void controlLed() 
{
    ledPauseOn = !errorCode;
    if( errorCode ) ledToReport = errorCode;
    else ledToReport = config.useDS ? wire.count() : 3;
}

int8_t calcNTCtemp( float r  ) // accepts resistance in K returns temperature C
{
    return 1.0f / (  log( r / ( config.use100K ? 100.0f : 10.0f ) ) / float( config.beta ) + 1/298.15f ) - 273.15f;
}

void readTemperatures()
{
    static bool sendRequest = true;
    static unsigned long time = 0;
    auto ltime = millis();
    unsigned delta = ltime - time;
    
    if( errorCode ) {
        if( delta >= 500 ) {
            initTherms();
            if( errorCode ) {
                time = ltime;
                return;
            }
            sendRequest = true;
        } else 
            return;
    }

    if( sendRequest ) {
        if( delta >= 500 ) {
            if( config.useDS ) {
                wire.beginConvert();
                sendRequest = false;
            } else {
                // measure temp on NTC
                float r = 0.0f;
                errorCode = readAnalogResistance( &r );
                if( errorCode ) {
                    controlLed();
                    time = ltime;
                    return;
                }
                temperatures[0] = calcNTCtemp( r );
                temperatures[1] = -127;
            }
            time = ltime;
        }
    } else {
        if( delta >= 120 ) { // enough for 9 bit, which should be 93.75
            for( uint8_t i = 0; i < wire.count(); ++i ) {
                char temp = 0;
                errorCode = wire.getTemperature( i, temp );
                if( errorCode ) {
                    controlLed();
                    break;
                }
                temperatures[i] = temp;
            }
            time = ltime;
            sendRequest = true;
        }
    }

}

void loop()
{
    processCommand();
    readTemperatures();
    controlFan();
    delay(20);
}

